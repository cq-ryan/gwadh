gwadh
=====

A utility to capture and analyze LabOne data (especially for `HDAWG`s).

## Building

`cargo build --release`

You must also give the compiled binary the capability to capture packets on your system.

`sudo setcap cap_net_raw,cap_net_admin=eip target/release/gwadh`

## Running

To see options, run:

`./gwadh -h`

To enable more verbose logging, set environment variable `RUST_LOG=info` or `RUST_LOG=debug`.
