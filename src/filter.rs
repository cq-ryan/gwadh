use std::fmt;
use std::net::IpAddr;

use pnet::packet::ethernet::{EtherTypes, EthernetPacket};
use pnet::packet::ip::IpNextHeaderProtocols;
use pnet::packet::ipv4::Ipv4Packet;
use pnet::packet::tcp::TcpPacket;
use pnet::packet::Packet;

/// Direction of packet transmission
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum PacketSource {
    /// Client -> Server
    Client,
    /// Server -> Client
    Server,
}

/// Captured packet for a `LabOneFilter`
#[derive(Clone, Debug)]
pub struct LabOneFilterMatch {
    /// Direction of transmission
    pub source: PacketSource,
    /// TCP flags
    pub flags: u16,
    /// Address of client in format `<ip>:port`
    pub client_addr: String,
    /// TCP payload of the match
    pub payload: Option<Vec<u8>>,
}

impl fmt::Display for LabOneFilterMatch {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let payload_len = match &self.payload {
            Some(p) => p.len(),
            None => 0,
        };

        write!(
            f,
            "LabOneFilterMatch<{:?}> flags={}, client_addr={}, payload.len()={}",
            self.source, self.flags, self.client_addr, payload_len
        )
    }
}

/// Packet capture filter for LabOne traffic
#[derive(Clone, Copy, Debug)]
pub struct LabOneFilter {
    labone_ip: IpAddr,
    labone_port: u16,
}

impl LabOneFilter {
    /// Instantiate a new LabOneFilter
    pub fn new(labone_ip: IpAddr, labone_port: u16) -> Self {
        Self {
            labone_ip,
            labone_port,
        }
    }

    /// Parse an Ethernet packet and return a `LabOneFilterMatch` if it appears
    /// to be LabOne traffic
    pub fn parse(&self, ethernet: &EthernetPacket) -> Option<LabOneFilterMatch> {
        if ethernet.get_ethertype() == EtherTypes::Ipv4 {
            self.parse_ipv4(&Ipv4Packet::new(ethernet.payload())?)
        } else {
            None
        }
    }

    fn parse_ipv4(&self, ipv4_packet: &Ipv4Packet) -> Option<LabOneFilterMatch> {
        if ipv4_packet.get_next_level_protocol() != IpNextHeaderProtocols::Tcp {
            return None;
        }

        let (maybe_source, cli_ip) = {
            let (source, dest) = (
                IpAddr::V4(ipv4_packet.get_source()),
                IpAddr::V4(ipv4_packet.get_destination()),
            );

            if self.labone_ip == source {
                Some((PacketSource::Server, dest))
            } else if self.labone_ip == dest {
                Some((PacketSource::Client, source))
            } else {
                None
            }
        }?;

        let tcp = TcpPacket::new(ipv4_packet.payload())?;

        let filter_match = {
            let (source_port, dest_port) = (tcp.get_source(), tcp.get_destination());

            match maybe_source {
                PacketSource::Server => {
                    if source_port == self.labone_port {
                        Some(dest_port)
                    } else {
                        None
                    }
                }
                PacketSource::Client => {
                    if dest_port == self.labone_port {
                        Some(source_port)
                    } else {
                        None
                    }
                }
            }
        };

        filter_match.map(|cli_port| {
            let payload = {
                let payload = tcp.payload();

                if payload.is_empty() {
                    None
                } else {
                    Some(Vec::from(payload))
                }
            };
            LabOneFilterMatch {
                flags: tcp.get_flags(),
                client_addr: format!("{}:{}", cli_ip, cli_port),
                source: maybe_source,
                payload,
            }
        })
    }
}
