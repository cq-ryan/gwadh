use std::convert::TryFrom;

/// Errors that may be encountered when unpacking a packet.
#[derive(Debug, Clone)]
pub enum UnpackError {
    /// The packet was smaller than anticipated.
    Underflow(usize, usize),
    /// The packet was not the same size as anticipated.
    ///
    /// This could probably be refactored to be an `Underflow`
    /// everywhere that it is used.
    PacketSizeError(usize, usize),
}

impl std::fmt::Display for UnpackError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::Underflow(actual, expected) => {
                write!(f, "expected {} bytes, got {} bytes", expected, actual)
            }
            Self::PacketSizeError(actual, expected) => {
                write!(
                    f,
                    "expected packet of {} bytes, got {} bytes",
                    expected, actual
                )
            }
        }
    }
}

/// Header for a `RequestPacket`.
#[derive(Debug, Clone)]
pub struct RequestHeader {
    /// Total size of a packet
    pub packet_size: u32,
    /// Expected size of the string path
    pub path_size: u16,
    /// Unknown/maybe reserved. Seems to always be 0.
    pub unknown: u16,
    /// ID of this request.
    ///
    /// This ID is usually unique unless the payload takes multiple packets to
    /// transmit (usually a `RequestVectorSlice`)
    pub req_id: u16,
    /// Type of request.
    ///
    /// TODO: This should be an enum.
    pub maybe_enum: u32,
}

impl RequestHeader {
    const SIZE: usize = 12;
}

impl TryFrom<&[u8]> for RequestHeader {
    type Error = UnpackError;

    fn try_from(buf: &[u8]) -> Result<Self, Self::Error> {
        if buf.len() < Self::SIZE {
            Err(UnpackError::Underflow(buf.len(), Self::SIZE))
        } else {
            debug!("Header: {:?}", &buf[..Self::SIZE]);
            let packet_size = u32::from_le_bytes(buf[4..8].try_into().unwrap());
            let path_size = if (packet_size as usize) < Self::SIZE + 2 {
                0
            } else {
                u16::from_le_bytes(buf[12..14].try_into().unwrap())
            };

            Ok(Self {
                maybe_enum: u32::from_le_bytes(buf[0..4].try_into().unwrap()),
                packet_size,
                req_id: u16::from_le_bytes(buf[8..10].try_into().unwrap()),
                unknown: u16::from_le_bytes(buf[10..12].try_into().unwrap()),
                path_size,
            })
        }
    }
}

/// Vector argument.
#[derive(Debug, Clone)]
pub struct RequestVectorSlice {
    /// Total number of elements.
    pub total_size: u64,
    /// Elements that have been transferred from client to server thus far.
    pub num_elems_so_far: u64,
    /// Elements in this request.
    pub size_this_slice: u32,
    /// Type of element.
    ///
    /// TODO: this should be an enum.
    pub vec_type: u8,
}

impl RequestVectorSlice {
    const HEADER_SIZE: usize = 21;
}

impl TryFrom<&[u8]> for RequestVectorSlice {
    type Error = UnpackError;

    fn try_from(buf: &[u8]) -> Result<Self, Self::Error> {
        if buf.len() < Self::HEADER_SIZE {
            return Err(UnpackError::Underflow(buf.len(), Self::HEADER_SIZE));
        }

        let packet = Self {
            total_size: u64::from_le_bytes(buf[..8].try_into().unwrap()),
            num_elems_so_far: u64::from_le_bytes(buf[8..16].try_into().unwrap()),
            size_this_slice: u32::from_le_bytes(buf[16..20].try_into().unwrap()),
            vec_type: buf[20],
        };

        let expected_packet_size = {
            let element_size: usize = 1 << packet.vec_type;
            let payload_size: usize = packet.size_this_slice as usize * element_size;
            payload_size + Self::HEADER_SIZE
        };

        if expected_packet_size != buf.len() {
            warn!(
                "Expected vector packet of size {}, got {}",
                expected_packet_size,
                buf.len()
            )
        }

        Ok(packet)
    }
}

/// Payload argument for a given request
#[derive(Debug, Clone)]
pub enum RequestPayload {
    /// String argument.
    ///
    /// An argument of this type may actually be a
    /// `Bytes` type if it happens to contain ASCII characters.
    Str(String),
    /// Integer argument.
    Int(i64),
    /// Bytes argument.
    Bytes(Vec<u8>),
    /// Double / float argument.
    Double(f64),
    /// Vector argument.
    Vector(RequestVectorSlice),
    /// Unknown or unexpected argument.
    Unexpected(Vec<u8>),
}

impl RequestPayload {
    fn unpack(req_enum: u32, buf: &[u8]) -> Self {
        match req_enum {
            7 => Self::Int(u64::from_le_bytes(buf[..8].try_into().unwrap()) as i64),
            8 => Self::Double(f64::from_le_bytes(buf[..8].try_into().unwrap())),
            9 => {
                if buf.len() < 4 {
                    warn!("Buffer underflow while unpacking string or bytes.");
                    return Self::Unexpected(Vec::from(buf));
                }
                let payload_len = {
                    let payload_len = u32::from_le_bytes(buf[..4].try_into().unwrap());
                    payload_len as usize
                };
                if payload_len + 4 != buf.len() {
                    warn!(
                        "String or byte packet size not equal to actual buffer size: {} != {}",
                        payload_len + 4,
                        buf.len()
                    );
                    return Self::Unexpected(Vec::from(buf));
                }
                let payload = &buf[4..];

                // Guess if this is a string or byte array
                let is_str = {
                    let non_ascii = payload
                        .iter()
                        .find(|x| !(x.is_ascii_punctuation() || x.is_ascii_alphanumeric()));
                    non_ascii.is_none()
                };

                if is_str {
                    Self::Str(String::from_utf8_lossy(payload).into())
                } else {
                    Self::Bytes(Vec::from(payload))
                }
            }
            31 => Self::Vector(RequestVectorSlice::try_from(buf).unwrap()),
            _ => Self::Unexpected(Vec::from(buf)),
        }
    }
}

/// Structure for LabOne requests (client -> server)
#[derive(Debug, Clone)]
pub struct RequestPacket {
    /// Header information for the packet
    pub header: RequestHeader,
    /// Path of the request
    pub path: String,
    /// Arguments for the request
    pub arguments: Option<RequestPayload>,
}

impl RequestPacket {
    /// Get the size of a Request in a TCP payload
    pub fn buffer_size(&self) -> usize {
        self.header.packet_size as usize
    }
}

impl TryFrom<&[u8]> for RequestPacket {
    type Error = UnpackError;

    fn try_from(buf: &[u8]) -> Result<Self, Self::Error> {
        let header = RequestHeader::try_from(buf)?;
        let header_packet_size = header.packet_size as usize;

        if buf.len() < header_packet_size {
            return Err(UnpackError::Underflow(buf.len(), header_packet_size));
        }

        let path_and_header_size = RequestHeader::SIZE
            + if header.path_size > 0 {
                2 + header.path_size as usize
            } else {
                0
            };

        if buf.len() < path_and_header_size {
            return Err(UnpackError::Underflow(buf.len(), path_and_header_size));
        }

        let path = if header.path_size > 0 {
            String::from_utf8_lossy(&buf[RequestHeader::SIZE + 2..path_and_header_size]).into()
        } else {
            "".into()
        };

        let arguments = {
            if header_packet_size > path_and_header_size {
                let bytes = &buf[path_and_header_size..header_packet_size];
                Some(RequestPayload::unpack(header.maybe_enum, bytes))
            } else {
                None
            }
        };

        Ok(Self {
            header,
            path,
            arguments,
        })
    }
}
