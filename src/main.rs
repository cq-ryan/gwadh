use gwadh::connection::ConnectionManager;
use gwadh::filter::LabOneFilter;

#[macro_use]
extern crate log;

use clap::Parser;

use pnet::datalink::{self, NetworkInterface};

use pnet::packet::ethernet::EthernetPacket;

/// Traffic analyzer for ZI clients and LabOne servers
#[derive(Parser, Debug, Clone)]
#[clap(author, version, about, long_about = None)]
struct Config {
    /// Network Interface
    #[clap(short, long, value_parser, default_value = "any")]
    interface: String,

    /// LabOne IP
    #[clap(long, value_parser, default_value = "127.0.0.1")]
    labone_ip: String,

    /// Labone Port
    #[clap(long, value_parser, default_value_t = 8004)]
    labone_port: usize,

    /// Buffer size
    #[clap(short, long, value_parser, default_value_t = 8196)]
    buffer_size: usize,
}

impl Config {
    fn get_interface(&self) -> Option<NetworkInterface> {
        let interface_name_matches = |iface: &NetworkInterface| iface.name == self.interface;
        let interfaces = datalink::interfaces();

        interfaces.into_iter().find(interface_name_matches)
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    use pnet::datalink::Channel::Ethernet;

    let args = Config::parse();

    pretty_env_logger::init();

    let interface = args
        .get_interface()
        .unwrap_or_else(|| panic!("Could not find network interface {}", args.interface));

    // Create a channel to receive on
    let cfg = datalink::Config {
        write_buffer_size: args.buffer_size,
        read_buffer_size: args.buffer_size,
        ..Default::default()
    };

    let (_, mut rx) = match datalink::channel(&interface, cfg) {
        Ok(Ethernet(tx, rx)) => (tx, rx),
        Ok(_) => panic!("packetdump: unhandled channel type"),
        Err(e) => panic!("packetdump: unable to create channel: {}", e),
    };

    let filter = LabOneFilter::new(args.labone_ip.parse()?, args.labone_port as u16);
    let mut conn = ConnectionManager::default();

    loop {
        match rx.next().map(EthernetPacket::new) {
            Ok(Some(packet)) => {
                if let Some(labone_packet) = filter.parse(&packet) {
                    conn.update(labone_packet);
                } else {
                    conn.poll();
                }
            }
            Err(e) => error!("unable to receive packet: {}", e),
            _ => error!("unable to receive packet"),
        }
    }
}
