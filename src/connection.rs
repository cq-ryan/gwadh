use crate::filter::{LabOneFilterMatch, PacketSource};
use crate::packet::{RequestPacket, RequestPayload};

use std::collections::HashMap;
use std::time::{Duration, Instant};

use pnet::packet::tcp::TcpFlags;

/// State of the client / server connection
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum ConnectionState {
    /// Connection in progress
    Connecting,
    /// Connection active
    Open,
    /// Connection has been closed
    Closed,
    /// Connection is in an unknown or error state
    Error,
}

/// Client / server connection handle
#[derive(Clone, Debug)]
pub struct Connection {
    state: ConnectionState,
    last_updated: Instant,
    stack: (Option<u16>, Option<u16>, Option<u16>),
    requests: Vec<RequestPacket>,
    cli_buffer: Vec<u8>,
}

impl Connection {
    /// Instantiate a new connection
    pub fn new() -> Self {
        Self {
            state: ConnectionState::Connecting,
            stack: (None, None, None),
            last_updated: Instant::now(),
            requests: Vec::new(),
            cli_buffer: Vec::new(),
        }
    }

    fn update_client(&mut self, packet: LabOneFilterMatch) {
        if let Some(ref mut data) = packet.payload.clone() {
            self.cli_buffer.append(data);
        }

        if packet.flags & TcpFlags::PSH == 0 {
            return;
        }

        // If we have gotten this far, the packet has fully transferred
        let data = self.cli_buffer.clone();
        self.cli_buffer = Vec::new();

        debug!(
            "[{:p}] packet of size {} with flags {}",
            self,
            data.len(),
            packet.flags
        );

        let mut offset: usize = 0;
        loop {
            if offset == data.len() {
                break;
            }

            match RequestPacket::try_from(&data[offset..]) {
                Ok(packet) => {
                    offset += packet.buffer_size();
                    if packet.buffer_size() > 0 {
                        info!("[{:p}:{}] {:?}", self, offset, packet);
                        self.requests.push(packet);
                    }
                }
                Err(e) => {
                    if !String::from_utf8_lossy(&data[offset..]).starts_with("proto") {
                        warn!("[{:p}:{}] could not handle data type: {}", self, offset, e);
                        self.cli_buffer = data[offset..].into();
                    }
                    break;
                }
            }
        }
    }

    /// Update connection state with a new match from the LabOne filter
    pub fn update(&mut self, packet: LabOneFilterMatch) {
        use ConnectionState::{Closed, Connecting, Error, Open};
        use TcpFlags::{ACK, FIN, SYN};
        const SYN_ACK: u16 = SYN | ACK;
        const FIN_ACK: u16 = FIN | ACK;

        self.last_updated = Instant::now();
        self.stack = (self.stack.1, self.stack.2, Some(packet.flags));

        if packet.source == PacketSource::Client {
            self.update_client(packet);
        }

        self.state = match self.state {
            Connecting => {
                let syn = self.stack.0.map(|s| s & SYN);
                let syn_ack = self.stack.1.map(|s| s & (SYN_ACK));
                let ack = self.stack.2.map(|s| s & ACK);

                match (syn, syn_ack, ack) {
                    (Some(SYN), Some(SYN_ACK), Some(ACK)) => Open,
                    _ => Connecting,
                }
            }
            Open => {
                let fin = self.stack.0.map(|s| s & FIN);
                let fin_ack = self.stack.1.map(|s| s & (FIN_ACK));
                let ack = self.stack.2.map(|s| s & ACK);

                match (fin, fin_ack, ack) {
                    (Some(FIN), Some(FIN_ACK), Some(ACK)) => Closed,
                    _ => Open,
                }
            }
            Closed => {
                error!("Packet sent after connection closed.");
                Error
            }
            Error => Error,
        }
    }
}

impl Default for Connection {
    fn default() -> Self {
        Self::new()
    }
}

impl Drop for Connection {
    fn drop(&mut self) {
        let mut vec_reqs: HashMap<u16, Vec<&RequestPacket>> = HashMap::new();

        for vec_req in self.requests.iter() {
            if vec_req.header.maybe_enum == 31 {
                let id = vec_req.header.req_id;

                let vecs = vec_reqs.entry(id).or_default();
                vecs.push(vec_req)
            }
        }

        for (req_id, reqs) in vec_reqs.iter() {
            let mut transmitted_elems = 0_u64;
            let mut expected_transmitted_elems = 0_u64;

            for (i, req) in reqs.iter().enumerate() {
                if let Some(RequestPayload::Vector(vec)) = &req.arguments {
                    transmitted_elems += vec.size_this_slice as u64;

                    if i == 0 {
                        expected_transmitted_elems = vec.total_size;
                    } else if expected_transmitted_elems != vec.total_size {
                        warn!(
                            "Total elements changed in the middle of a request! {} => {}",
                            expected_transmitted_elems, vec.total_size
                        );
                    }
                }
            }

            if transmitted_elems != expected_transmitted_elems {
                warn!(
                    "Not all elements for request {} transferred! {}/{}",
                    req_id, transmitted_elems, expected_transmitted_elems
                );
            } else {
                info!(
                    "All elements for request {} transferred ({})",
                    req_id, expected_transmitted_elems
                );
            }
        }
    }
}

/// Manager for several simultaneous connections
pub struct ConnectionManager {
    conns: HashMap<String, Connection>,
    ttl: Duration,
}

impl ConnectionManager {
    /// Instantiate a new ConnectionManager
    ///
    /// * `ttl` - Time To Live before dropping connection
    pub fn new(ttl: Duration) -> Self {
        Self {
            ttl,
            conns: HashMap::new(),
        }
    }

    /// Update connection manager with a new match from the LabOne filter
    pub fn update(&mut self, packet: LabOneFilterMatch) {
        let client_addr = packet.client_addr.clone();

        let conn = self.conns.entry(client_addr.clone()).or_default();

        let last_state = conn.state;
        conn.update(packet);
        let this_state = conn.state;

        use ConnectionState::{Closed, Connecting, Error, Open};
        match (last_state, this_state) {
            (Connecting, Open) => {
                info!("[{}] New connection", client_addr);
            }
            (Open, Closed) => {
                info!("[{}] Closed connection", client_addr);
            }
            (last_state, Error) => {
                if last_state != Error {
                    error!("[{}] Connection in error state", client_addr)
                }
            }
            _ => {}
        }
    }

    /// Poll connection manager and drop connections that have exceeded TTL
    pub fn poll(&mut self) {
        let timeout = Instant::now() - self.ttl;
        self.conns.retain(|_, c| c.last_updated >= timeout);
    }
}

impl Default for ConnectionManager {
    fn default() -> Self {
        Self::new(Duration::from_secs(15))
    }
}
